﻿using Migration49.Interfeces;
using Migration49.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork40.Models
{
    public class Post : IEntity<int>
    {
        public int PostID { get; set; }

        [ForeignKey("User")]
        public int ID { get; set; }
        public int Comment { get; set; }
        public virtual User User { get; set; }
    }
}
