﻿using HomeWork40.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Migration49.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migration49
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Users\User\GitRepositories\hw40\HomeWork40");
            builder.AddJsonFile("appSettings.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}
