﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migration49.Interfeces
{
    interface IEntity<T>
    {
        public T ID { get; set; }
    }
}
